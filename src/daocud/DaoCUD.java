
package daocud;

/**
 *
 * @author rsmon
 */
public interface DaoCUD {
    
   void enregistrerEntite (Object entite);
   void supprimerEntite   (Object entite);
   void repercuterMAJ     (Object entite);
   
   void detacher          (Object entite);
   void rafraichir        (Object entite);
}
