package daocud;
import dao.orm.Em;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class DaoCUDImpl implements DaoCUD {
   
    @Inject @Em private EntityManager em;  
     
    @Override
    public void enregistrerEntite(Object entite) {
       
       em.getTransaction().begin();
       em.persist(entite);
       em.getTransaction().commit();
    }
    
    @Override
    public void supprimerEntite(Object entite) {
       
       em.getTransaction().begin();
       Object asupp=em.merge(entite);
       em.remove(asupp);
       em.getTransaction().commit();
    }
    
    @Override
    public void repercuterMAJ(Object entite) {
      
       em.getTransaction().begin();
       entite=em.merge(entite);
       em.getTransaction().commit();
    }

     @Override
    public void detacher(Object entite) {
      
       em.getTransaction().begin();    
       em.detach(entite);
       em.getTransaction().commit();
    }
     
      @Override
    public void rafraichir(Object entite) {
        
        em.getTransaction().begin();  
        em.refresh(entite);
        em.getTransaction().commit();
    }

}


