package dao.region;

import dao.orm.Em;
import entites.Region;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

/**
 *
 * @author rsmon
 */

@Singleton
public class DaoRegionImpl  implements DaoRegion,Serializable {

    @Inject @Em EntityManager em;
    
    @Override
    public List<Region> getToutesLesRegions() {
     
        return em.createQuery("Select r from Region r").getResultList();
    }  
}
