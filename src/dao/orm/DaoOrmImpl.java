package dao.orm;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author rsmon
 */


@Singleton
public class DaoOrmImpl  {
   
    @Produces @Em private EntityManager em;  
    
    public DaoOrmImpl() {
      
       em=Persistence.createEntityManagerFactory("PU").createEntityManager(); 
    }  
}


