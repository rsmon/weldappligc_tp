package dao.client;

import dao.orm.Em;
import entites.Client;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class DaoClientImpl implements DaoClient {   
    
    @Inject @Em EntityManager em;
    
    @Override
    public Client getLeClient(Long pNumcli) {
        
       return em.find(Client.class, pNumcli);
    }

    @Override
    public List<Client> getTousLesClients() {
       
        return em.createQuery("Select c From Client c order by c.nomCli").getResultList();
    }
}


