package dao.produit;
import dao.orm.Em;
import entites.Produit;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class DaoProduitImpl  implements DaoProduit, Serializable{

    @Inject @Em EntityManager em;
    
    
    @Override
    public Produit getProduit(String pRefProd) {
        return em.find(Produit.class, pRefProd);
    }    
}
