
package bal.commande;

import dao.tva.DaoTva;
import dto.commande.ResumeCommande;
import entites.Commande;
import entites.LigneDeCommande;
import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class BalCommandeImpl implements BalCommande{
       
    @Inject DaoTva      daoTva;
    
    @Override
    public Float montantCommandeHT(Commande pCommande) {
        
        Float montant=0F;
        if(pCommande!=null){
          for(LigneDeCommande lgdc: pCommande.getLesLignesDeCommande()){
        
           montant+=lgdc.getQteCom()*lgdc.getLeProduit().getPrixProd();
          }
        }else montant=null;
        return montant;
    } 
    
    @Override
    public Float montantCommandeTTC(Commande pCommande) {
         
        return montantCommandeHT(pCommande)*(1+daoTva.getTauxTVA());
    }
  
    @Override
    public boolean estReglee(Commande pCommande) {
        return pCommande.getEtatCom().equalsIgnoreCase("R");
    }
    
    
    @Override
    public ResumeCommande        getResumeCommande(Commande c){
    
       return new ResumeCommande(
                   
                 c.getNumCom(),
                 c.getDateCom(),
                 c.getEtatCom(),
                 montantCommandeHT(c),
                 montantCommandeTTC(c)       
             ) ;
    }
}
