package application.modele;

import bal.client.BalClient;
import dao.client.DaoClient;
import dto.commande.ResumeCommande;
import entites.Client;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ModeleCommandesParClientsImpl implements ModeleCommandesParClients {
   
   @Inject DaoClient           daoClient;
   @Inject BalClient           balClient;
   
   //<editor-fold defaultstate="collapsed" desc="DELEGATIONS">
   
   @Override
   public List<Client>         getTousLesClients() {
       
       return daoClient.getTousLesClients();
   }
   
 
    @Override
   public Client               getLeClient(Long pNumcli) {
       return daoClient.getLeClient(pNumcli);
   }
   
    @Override
    public Float caAnneeEnCours(Client pClient) {
        return balClient.caAnneeEnCours(pClient);
    }
   
   
   @Override
   public List<ResumeCommande> getResumesCommandes(Client client) {
       return balClient.getResumesCommandeClient(client);
   }
   //</editor-fold>

}


