package application.modele;

import dto.commande.ResumeCommande;
import entites.Client;
import java.util.List;

public interface ModeleCommandesParClients {

    List<Client>         getTousLesClients();
    List<ResumeCommande> getResumesCommandes(Client client);
    Float                caAnneeEnCours(Client pClient);
    
    //<editor-fold defaultstate="collapsed" desc="         ">
    Client               getLeClient(Long pNumcli);
    //</editor-fold>
}



