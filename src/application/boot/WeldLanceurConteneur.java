package application.boot;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 *
 * @author rsmon
 */

public class WeldLanceurConteneur {

    private final static  String APPLI_C1="cons.CDemo";
    
    private final static  String APPLI_S1="swing.SDemo";
    

    public static void main( String[] args ) throws ClassNotFoundException {
        
        fixeNiveauMessages(Level.SEVERE);
        execute( APPLI_C1);
    }
    
    private static void execute(String nomAppliWeld) throws ClassNotFoundException{
    
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
       
        Class appli=Class.forName("application.apps."+nomAppliWeld);
        
        AppliWeld appliWeld = (AppliWeld) container.instance().select(appli).get();
        appliWeld.run();
        
        weld.shutdown();
    
    }
    
     private static void fixeNiveauMessages(Level niveau) throws SecurityException {
   
        Handler systemOutHandler = new StreamHandler(System.out, new SimpleFormatter()); 
        systemOutHandler.setLevel(niveau); 
        Logger rootLogger = Logger.getLogger(""); 
        rootLogger.addHandler(systemOutHandler); 
        rootLogger.setLevel(niveau);
        
    }
    
}


