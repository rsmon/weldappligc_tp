package application.rendus;

import java.util.Date;
import javax.swing.table.DefaultTableCellRenderer;
import utilitaires.UtilDate;


  public class DateRenderer extends DefaultTableCellRenderer {

    public DateRenderer() {
      super();
      setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
    }

    @Override
    public void setValue(Object value) {
      if ((value != null) && (value instanceof Date)) {
       Date date=(Date)value; 
       value = UtilDate.format(date);
      }
      super.setValue(value);
    }

  }

