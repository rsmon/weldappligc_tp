package application.rendus;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import entites.Client;
import entites.Region;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author rsmon
 */

public class RenduRegion extends JLabel implements ListCellRenderer{

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Region c = (Region)value;
        JLabel renderer = new JLabel(c.getCodeRegion()+":"+c.getNomRegion() );
        if (isSelected) {
            renderer.setOpaque(true);
            renderer.setForeground(Color.BLUE);
            renderer.setBackground(Color.GRAY);
        }
        return renderer;
    }
    
}
    


