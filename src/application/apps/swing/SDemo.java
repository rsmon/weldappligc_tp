package application.apps.swing;
import application.boot.AppliWeld;
import application.modele.ModeleCommandesParClients;
import application.vues.VueSDemo;
import javax.inject.Inject;

public class SDemo implements AppliWeld{
       
    @Inject ModeleCommandesParClients modele;
      
    @Override
    public void run(){
         
          new VueSDemo(modele).setVisible(true);
          
    }
}


