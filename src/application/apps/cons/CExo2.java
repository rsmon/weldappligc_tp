/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package application.apps.cons;

import application.boot.AppliWeld;
import bal.client.BalClient;
import dao.client.DaoClient;
import entites.Client;
import javax.inject.Inject;

/**
 *
 * @author rsmon
 */
public class CExo2 implements AppliWeld{

    @Inject DaoClient dao;
    @Inject BalClient bal;
    
    @Override
    public void run() {
       
        for( Client c : dao.getTousLesClients()){
        
               c.afficher();
               System.out.printf("%8.2f€\n",bal.caAnneeEnCours(c));
        
        }
        
        
    }
    
}
