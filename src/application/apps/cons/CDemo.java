package application.apps.cons;

import application.boot.AppliWeld;
import application.modele.ModeleCommandesParClients;
import dto.commande.ResumeCommande;
import entites.Client;
import javax.inject.Inject;
import static utilitaires.UtilDate.format;

/**
 *
 * @author rsmon
 */

public class CDemo implements AppliWeld{
    
   @Inject ModeleCommandesParClients modele;
    
   @Override
   public void run(){
       
    Client cli= modele.getLeClient(101L);
       
    System.out.println();
        
    cli.afficher();
        
    System.out.println("\n");
    
    for(ResumeCommande rc:modele.getResumesCommandes(cli)){
     
         System.out.printf("%6d %10s %8.2f € %8.2f €  %1s \n",
                            rc.getNumcom(),
                            format(rc.getDatecom()),
                            rc.getMontantHT(),
                            rc.getMontantTTC(),
                            rc.getEtatcom()
                          ); 
    }
       
    System.out.printf("\n\n Chiffre d'Affaires Annuel:%10.2f €\n\n",modele.caAnneeEnCours(cli));
    
 }    
}
