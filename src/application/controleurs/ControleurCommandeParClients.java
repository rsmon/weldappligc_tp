package application.controleurs;

import application.modele.ModeleCommandesParClients;
import dto.commande.ResumeCommande;
import entites.Client;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedList;
import java.util.List;

public class ControleurCommandeParClients {
    
    private ModeleCommandesParClients modele;
    
    private List<Client>              lesClients      = new LinkedList<Client>();
    private Client                    clientSel       = null;
    private List<ResumeCommande>      lesCommandes    = new LinkedList<ResumeCommande>();
    private Float                     caAnnuelClient  = 0F;
      
    public void init(Object param){
       
       this.modele=(ModeleCommandesParClients)param;
       this.setLesClients(modele.getTousLesClients());          
    }  
    
    public void traiterSelectionCombo(){
    
      if(clientSel!=null){
          
           this.setLesCommandes(modele.getResumesCommandes(clientSel));
           this.setCaAnnuelClient(modele.caAnneeEnCours(clientSel));
      } 
    } 
    
    //<editor-fold defaultstate="collapsed" desc="CODE GENERE">
    
    public static final String PROP_LESCLIENTS = "lesClients";
    
    public List<Client> getLesClients() {
        return lesClients;
    }
    
    public void setLesClients(List<Client> lesClients) {
        List<Client> oldLesClients = this.lesClients;
        this.lesClients = lesClients;
        propertyChangeSupport.firePropertyChange(PROP_LESCLIENTS, oldLesClients, lesClients);
    }
    
    
    public static final String PROP_CLIENTSEL = "clientSel";
    
    public Client getClientSel() {
        return clientSel;
    }
    
    public void setClientSel(Client clientSel) {
        Client oldClientSel = this.clientSel;
        this.clientSel = clientSel;
        propertyChangeSupport.firePropertyChange(PROP_CLIENTSEL, oldClientSel, clientSel);
    }
    
    public static final String PROP_LESCOMMANDES = "lesCommandes";
    
    public List<ResumeCommande> getLesCommandes() {
        return lesCommandes;
    }
    
    public void setLesCommandes(List<ResumeCommande> lesCommandes) {
        List<ResumeCommande> oldLesCommandes = this.lesCommandes;
        this.lesCommandes = lesCommandes;
        propertyChangeSupport.firePropertyChange(PROP_LESCOMMANDES, oldLesCommandes, lesCommandes);
    }
    
    
    public static final String PROP_CAANNUELCLIENT = "caAnnuelClient";
    
    public Float getCaAnnuelClient() {
        return caAnnuelClient;
    }
    
    public void setCaAnnuelClient(Float caAnnuelClient) {
        Float oldCaAnnuelClient = this.caAnnuelClient;
        this.caAnnuelClient = caAnnuelClient;
        propertyChangeSupport.firePropertyChange(PROP_CAANNUELCLIENT, oldCaAnnuelClient, caAnnuelClient);
    }
    
    
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>

}



   